

CC=gcc
CFLAGS=$(shell perl -MExtUtils::Embed -e ccopts)
LDFLAGS=$(shell perl -MExtUtils::Embed -e ldopts)


sample.o:

sample: sample.o
	$(CC) -o $@ $< $(LDFLAGS)

test: sample
	./sample -e 'use strict; use warnings; use Data::Dumper; print Dumper [1,2,3];'

clean:
	-rm -rf sample.o sample *.o

